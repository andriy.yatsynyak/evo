# coding=utf-8
from django.conf.urls import url
from django.views.generic import TemplateView
from users import views

urlpatterns = [
    url(
        r'^add/$',
        views.add_user,
        name='add_user'
    ),
    url(
        r'^random_ids/$',
        views.random_users,
        name='random_ids'
    ),
    url(r'', TemplateView.as_view(template_name='base.html'),
        name='base'),
]
