# coding=utf-8
import random
from django.shortcuts import render_to_response
from django.views.decorators.http import require_POST
from users.models import User
from django.template import RequestContext


@require_POST
def add_user(request):
    if request.POST.get('add_user'):
        usr_name = request.POST['user_name']
        if usr_name:
            try:
               User.objects.get(name=usr_name)
            except User.DoesNotExist:
                new_usr = User.objects.create(name=usr_name)
                new_usr.save()
                return render_to_response('add.html', {'usr_name': new_usr.name}, RequestContext(request))
        return render_to_response('base.html', {'err': 'you need create anothers user'}, RequestContext(request))


def random_users(request):
    usr_count = User.objects.count()
    if usr_count < 4:
        return render_to_response('base.html', {'err': 'you need create more users'}, RequestContext(request))
    my_ids = User.objects.values_list('id', flat=True)
    rand_ids = random.sample(my_ids, 3)
    random_records = User.objects.filter(id__in=rand_ids)
    return render_to_response('random.html', {'users': random_records}, RequestContext(request))
