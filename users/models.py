# coding=utf-8
from django.db import models


class User(models.Model):
    name = models.CharField(unique=True, max_length=800)

    class Meta(object):
        verbose_name = u"user"
        verbose_name_plural = u"users"

    def __unicode__(self):
        return u'{}'.format(self.name)
